#include <stdio.h>
typedef int bool;
#define false 0
#define true 1

int count_char(char *string, char c) {
    int mot  = 0;
    for (int i = 0; string[i] != '\0'; i++) {
        if (string[i] == c) {
            mot++;
        }
    }

    return mot;
}

int count_words(char *string) {

    int compteur = 0;
    for (int i = 0; string[i] != '\0'; i++) {
        if (string[i] == ' ' ) {
            compteur++;

        }

}
    return compteur+1;

int count_words_better(char *string) {

    int compteur = 0;
    bool verif = false;
    for (int i = 0; string[i] != '\0'; i++) {
        if (string[i] != ' ' && !verif) {
            verif = true;
        }

        if ((string[i] == ' ' || string[i + 1] == '\0') && verif) {
            compteur++;
            verif = false;

        }

    }

    return compteur;

}



}

int main() {

    char tab[50];
 	char target_char;

    int compteur;
    printf("Saisir un mot : ");
    fgets(tab,sizeof(tab), stdin);
    printf("Saisir le caractère à compter : ");
    scanf(" %c", &target_char);
    compteur = count_char(tab, target_char);
    printf("Ce caractère apparaît %d fois dans la chaîne \n", compteur);
    printf("En utilisant une fonction simple : ce texte contient  %d mots\n", count_words(tab));
    printf("En utilisant une fonction bien amelioré : ce texte contient %d mots\n", count_words_better(tab));

    return 0;

}



