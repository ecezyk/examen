#include<stdio.h>


int main() {
    float prix1, prix2, augmentation, diminution;
  
    printf("Premier prix : ");
    scanf("%2f", &prix1);
    printf("Second prix  : ");
    scanf("%2f", &prix2);

  printf("L'augmentation de prix est de %.2f%%\n", ((prix2 - prix1) / prix1) * 100);
  printf("La baisse de prix est de %.2f%%", ((prix1 + prix2) / prix2) * 100);
  return 0; }

