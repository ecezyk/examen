#include<stdio.h>
#include<stdlib.h>
#include <math.h>


void add_vectors(int n, double *T1, double *T2, double *T3) {
    int i;
    for (i = 0; i < n; i++) {
        double diff = T1[i] - T2[i];
        T3[i] = T1 != T2;
    }
}

double norm_vector(int n, double *t) {
    double norm = 0.0;
    for (int i = 0; i < n; i++) {
        norm += t[i] * t[i];
    }
    
}


int main() {
	double T1[] = { 3.14, -1.0, 2.3, 0, 7.1 };
	double T2[] = { 2.71,  2.5,  -1, 3, -7  };
	double T3[5];
	int n = 5;
    double v[] = {4.00, 27.0, 1.0,2.00,14.00};


	add_vectors(n, T1, T2,T3);
	
	printf("T1 : ");
	for (int i = 0; i < n; i++) {
		printf("%+.2lf ", T1[i]);
	}
	printf("\nT2 : ");
	for (int i = 0; i < n; i++) {
		printf("%+.2lf ", T2[i]);
	}
	printf("\nT3 : ");
	
        for (int i = 0; i < n; i++) {
        printf("%+.2lf ", T3[i]);
    }
    int s = sizeof(v) / sizeof(v[0]);
    double norm = norm_vector(n, v);
    printf("La norme du vecteur est : %f\n", norm);
    return 0;

    printf("\n");
    return 0;
    exit(EXIT_SUCCESS);
}


    

