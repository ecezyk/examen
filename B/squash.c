#include<stdio.h>



void small_to_zero(int *t, int n, int val) {
  for (int i = 0; i < n; i++) {
        if (t[i] <= val) {
            t[i] = 0;
        }
    }
}


int main() {
    int tab[] = {1, 3, -1, 2, 7, 5, 12, 4, 4, 3, 0};
    int n = sizeof(tab) / sizeof(tab[0]);
    int val = 6;
    
  

    
    printf("Avant : ");
    for (int i = 0; i < n; i++) {
        printf("%d ", tab[i]);
    }
   

    small_to_zero(tab, n, val);

    printf("\nAprès : ");
    for (int i = 0; i < n; i++) {
        printf("%d ", tab[i]);
        
    }
    printf("\n");

    return 0;
   }
